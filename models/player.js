const mongoose = require('mongoose');

const playerSchema = new mongoose.Schema({
    uid: String,
    displayName: String,
    email: String,
    photoURL: String,
    idPhoto: String,
    friends: [{
        uid: String,
        displayName: String,
        photoURL: String
    }],
    recivedFriendshipReq: [{ 
        uid : String,
        displayName: String
       

    }], //Peticiones de amistad recibidas
    sendedFriendshipReq:[{
        uid: String, 
        displayName: String

    }],//Peticiones de amistad enviadas 
    partidasVisor: Number,
    aciertosVisor: Number,
    partidasPintor: Number,
    aciertosPintor: Number,
    version: {type: Number, default: 0},
    SocketId: String,
    state: {type: Boolean, default: false}
});
module.exports = mongoose.model("player", playerSchema);