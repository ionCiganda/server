const mongoose = require('mongoose');

const matchSchema = new mongoose.Schema({
    matchId: String,
    player1DisplayName: String,
    player2DisplayName: String,
    player1: String,
    player2: String,
    listoP1: Boolean,
    listoP2: Boolean,
    socketID1: String,
    socketID2: String,
    word: String,
    pista: String,
    name: String,
    password: String
});
module.exports = mongoose.model("drawmatch", matchSchema);