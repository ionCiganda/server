const mongoose = require('mongoose');

const wordSchema = new mongoose.Schema({
    tema: String,
    palabra: String,
}); 
module.exports = mongoose.model("word",wordSchema);