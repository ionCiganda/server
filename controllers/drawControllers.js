const creatematch = require('../models/match');

exports.getMatchByid = (req, res) => {
    let matchId = req.params.id;
    creatematch.find({ _id: matchId }, (err, result) => {
        if (err) return res.status(500).send({ message: `getMatchByid => Error al realizar la petición: ${err}` });
        if (!result) return res.status(404).send({ message: `getMatchByid => No existe la partda` });
        console.log(`getMatchByid  match ${matchId} found successfully`);
        res.send({ match_Id: result });
    });
};
exports.DeleteMatchByid = function (req, res) {
    let matchId = req.params.id;
    creatematch.findByIdAndDelete({ _id: matchId }, (err, result) => {
        if (err) return res.status(500).send({ message: `DeleteMatchByid => Error al realizar la petición: ${err}` });
        if (!result) return res.status(404).send({ message: `DeleteMatchByid => No existe la partda` });
        console.log(`DeleteMatchByid => ${matchId} match deleted successfully`);
        res.send({ Deleted: result });
    });
};
exports.putMatch = function (req, res) {
    let id = req.params.id;
    let update = req.body;
    creatematch.findByIdAndUpdate(id, { $set: update }, { new: true }, function (err, result) {
        if (err) {
            return res.send(`Server error ${err}`);
        }
        res.send(result);
    });
};

//llamamos a la funcion enviandole el id del jugador, ahora será el uuid, posteriormente será el usuario(cuando haya logeos)
exports.searchOrCreateMatch = function (req, res) {
    let playerId = req.body.playerid;
    let socketid = req.body.socketid;
    let playerDisplayName = req.body.displayName;
    console.log(playerId + "-" + socketid);
    creatematch.findOne({ $or: [{ player1: playerId }, { player2: playerId }] }, (err, result) => {
        if (result) {
            console.log("user is allready in game ")
        }
        else {
            creatematch.findOneAndUpdate({ player2: null, name: null, password: null }, { player2: playerId, socketID2: socketid, player2DisplayName: playerDisplayName }, { new: true }, function (err, result) {
                //Si da algun error
                if (err) {
                    return res.send(`Server error ${err}`);
                }
                //si no encuenta, crea una nueva partida con la id del jugador como jugador1
                if (!result) {
                    console.log("nueva partida")
                    let newMatch = new creatematch();
                    //editado ** Julen **
                    Object.assign(newMatch, {
                        player1DisplayName: playerDisplayName,
                        player2DisplayName: null,
                        player1: playerId,
                        player2: null,
                        listoP1: false,
                        listoP2: false,
                        socketID1: socketid,
                        socketID2: null,
                        word: null,
                        pista: null,
                        name: null,
                        password: null,
                    });
                    newMatch.save()
                        .then((newMatch) => {
                            res.send(newMatch);
                        })
                        .catch(err => console.error('Error al guardar la nueva partidaaa: ', err))
                }
                else {
                    ioConnection.to(`${result.socketID1}`).emit('matchfull', { result: result, navigate: '/prematch/' });
                    ioConnection.to(`${result.socketID2}`).emit('matchfull', { result: result, navigate: '/prematch/' });
                }
            });
        }
    });
};

exports.createPrivateMatch = (req, res) => {
    let gamePass = req.body.password;
    let gameName = req.body.name;
    let playerid = req.body.playerid;
    let socketPID = req.body.socketid;
    let player1DisplayName = req.body.displayName
    let newMatch = new creatematch();

    Object.assign(newMatch, {
        player1DisplayName: player1DisplayName,
        player2DisplayName: null,
        player1: playerid,
        player2: null,
        socketID1: socketPID,
        socketID2: null,
        listoP1: false,
        listoP2: false,
        word: null,
        name: gameName,
        password: gamePass
    })
    newMatch.save()
        .then((newMatch) => {
            console.log(` createPrivateMatch => New Private Mtach created by ${playerid}`);
            res.send(newMatch);

        })
        .catch(err => console.log('Error al crear la partida privada:', err))

}
exports.joinPrivateMatch = (req, res) => {
    let gamePass = req.body.password;
    let gameName = req.body.name;
    let playerid = req.body.playerid;
    let socketPID = req.body.socketid;
    let player2DisplayName = req.body.displayName

    creatematch.findOneAndUpdate({ player2: null, name: gameName, password: gamePass }, {
        player2DisplayName: player2DisplayName,
        player2: playerid,
        socketID2: socketPID
    }, { new: true }, function (err, result) {
        if (result) {
            console.log(`joinPrivateMatch => ${playerid} joined successfully ${gameName} game`)
            ioConnection.to(`${result.socketID1}`).emit('matchfull', { result: result, navigate: '/prematch/' });
            ioConnection.to(`${result.socketID2}`).emit('matchfull', { result: result, navigate: '/prematch/' });
            res.send(result);
            return;
        };
        if (err) {
            return res.send('joinPrivateMatch something went wrong', err)
        }
        else {
            console.log(`joinPrivateMatch Else: player:${playerid} set wrong name or password `)
            ioConnection.to(`${socketPID}`).emit('sadsmiley')
        }
    });
}
