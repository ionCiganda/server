const createplayer = require('../models/player');

exports.postPlayer = (req, res) => {
    let newPlayer = new createplayer();
    Object.assign(newPlayer, req.body);
    newPlayer.save()
        .then((newPlayer) => {
            res.send(newPlayer);
        })
        .catch(err => console.error('Error al guardar el nuevo jugador: ', err))
};


exports.getPlayerByid = (req, res) => {
    let playerId = req.params.id;
    createplayer.find({ _id: playerId }, (err, result) => {
        if (err) return res.status(500).send({ message: `Error al realizar la petición: ${err}` });
        if (!result) return res.status(404).send({ message: `No existe el jugador` });
        console.log(`getPlayerByid ${playerId} successfully got player data`)
        res.send({ player_Id: result });
    });
};

exports.DeletePlayerByid = function (req, res) {
    let playerId = req.params.id;
    createplayer.findByIdAndDelete({ _id: playerId }, (err, result) => {
        if (err) return res.status(500).send({ message: `Error al realizar la petición: ${err}` });
        if (!result) return res.status(404).send({ message: `No existe el jugador` });
        console.log(`Deleted correctly`)
        res.send({ Deleted: result });
    });
};
exports.putPlayer = function (req, res) {
    let id = req.params.id;
    let update = req.body;
    createplayer.findByIdAndUpdate(id, { $set: update }, { new: true }, function (err, result) {
        if (err) {
            return res.send(`Server error ${err}`);
        }
        res.send(result);
    });
};

exports.postNewUser = (req, res) => {
    var user = JSON.parse(req.body.user)
    createplayer.findOne({ uid: user.uid }, { new: true }, function (err, result) {
        if (!result) {
            let newUser = new createplayer();
            Object.assign(newUser, user);
            newUser.save()
                .then(() => {
                    console.log(`postNewUser => New user ${user.uid} created`);
                    res.send(newUser);
                })
                .catch(err => console.error('newUser post error: ', err))
        }
        if (result) {
            console.log(` postNewUser => user already exists [${user.uid}:${user.displayName}]`);
            createplayer.findById(result, function (err, player) {
                if (err) {
                    return res.send(`Server error ${err}`);
                }
                console.log(`postNewUser sending player data`);
                res.send(player);
            });
        }
        if (err) {
            return res.send(`postNewUser => Something went wrong: ${err}`);
        }
    })
}
//test
exports.editPlayerName = function (req, res) {
    let user = req.params.uid
    let value = req.body.value

    createplayer.findOneAndUpdate({ uid: user }, { displayName: value }, { new: true }, function (err, result) {
        if (!err) {
            console.log(`editPlayerName \nuser: ${user} succesfully updated name to ${value}`)
            friends = result.friends;
            friendsUid = [];
            data = result;

            for (i = 0; i < friends.length; i++) {
                friendsUid.push(friends[i].uid)
            }
            console.log(`editPlayerName \n ${user} friends:${friendsUid}`);
            createplayer.updateMany({ uid: { $in: friendsUid } },
                { $set: { "friends.$[elem].displayName": value } },
                { new: true, arrayFilters: [{ "elem.uid": user }] }, function (err, response) {
                    if (response) {
                        console.log(`editPlayerName \ndisplayName:${value} friend updated on: ${friendsUid} users`);
                        res.send(data);
                        return;
                    }
                    if (!response) {
                        console.log('editPlayerName => No response');
                        res.sendStatus(404)
                        return;
                    }
                    if (err) {
                        console.log(`editPLayerName => Something went wrong P2 ERR: ${err}`);
                        res.send(err);
                        return;
                    }
                })



            //res.send(result)
        }
        else if (err) {
            console.log(`editPLayerName => Something went wrong ERR: ${err}`);
            return res.send(`Server error ${err}`);
        }
    });
}
function postRecived(activeUid, activeName, pasiveUid, pasiveName) {
    return new Promise((resolve, reject) => {
        createplayer.findOneAndUpdate({ uid: pasiveUid }, {
            $push: {
                recivedFriendshipReq: {
                    uid: activeUid,
                    displayName: activeName
                }
            },
            $inc: { version: 1 }
        },
            { new: true }, (err, response) => {
                if (response) {
                    console.log(response)
                    if (response.SocketId) {
                        ioConnection.to(`${response.SocketId}`).emit('newFriendRequest', { activeName });
                        console.log('entra en tengo socket con este sockect.id ' + response.SocketId +
                            'este quiere ser mi amgios ' + activeName)
                    }
                    else {
                        console.log('entrar a no tengo socket')
                    }
                    return resolve(response);

                } else if (!response) {
                    console.log(`postReceived => player:${pasiveUid} to update not found `)
                    return;
                } else {
                    console.log(`postReceived => ERR: ${err}`)
                    reject(err)
                }
            })
    })
};
//Function for post sended request
function postSended(activeUid, activeName, pasiveUid, pasiveName) {
    return new Promise((resolve, reject) => {
        createplayer.findOneAndUpdate({ uid: activeUid }, {
            $push: {
                sendedFriendshipReq: {
                    uid: pasiveUid,
                    displayName: pasiveName
                }
            }
        },
            { new: true }, (err, response) => {
                if (response) {

                    return resolve(response);
                } else if (!response) {
                    (`postSended => player:${pasiveUid} to update not found `)
                    return resolve(null);
                } else {
                    console.log(` postSended  => something went wrong ${err}`)
                    reject(err)
                }
            })
    })
};
//Function for delete sended request
function deleteSended(activeUid, activeName, pasiveUid, pasiveName) {
    return new Promise((resolve, reject) => {
        createplayer.findOneAndUpdate({ uid: pasiveUid }, {

            $pull: {
                sendedFriendshipReq: {
                    uid: activeUid,
                    displayName: activeName
                }
            }
        },
            { new: true }, (err, response) => {
                if (response) {
                    return resolve(response);
                } else if (!response) {
                    console.log(`deleteSended => Update failed`)
                    return resolve(null);
                } else {
                    console.log(` deleleSended => something went wrong  ${err}`)
                    reject(err)
                }
            })
    })
};
//Function for delete recived request
function deleteRecived(activeUid, activeName, pasiveUid, pasiveName) {
    return new Promise((resolve, reject) => {
        createplayer.findOneAndUpdate({ uid: activeUid }, {
            $pull: {
                recivedFriendshipReq: {
                    uid: pasiveUid,
                    displayName: pasiveName
                }
            },
            $inc: { version: -1 }
        },
            { new: true }, (err, response) => {
                if (response) {
                    return resolve(response);

                } else if (!response) {
                    console.log(`deleteRecived => Update failed`)
                    return resolve(null);
                } else {
                    console.log(`deleteRecived => Something went wrong ${err}`)
                    reject(err)
                }
            })
    })
};
function getAvoidMultipleFRequest(ownUid, friendUid) {

    return new Promise((resolve, reject) => {
        createplayer.findOne({ uid: ownUid }, function (err, result) {
            if (result) {

                x = result.sendedFriendshipReq

                if (x.some(e => e.uid === friendUid)) {
                    console.log(` getAvoidMultipleFRequest ERR: ${ownUid} already has done a request to ${friendUid}`);
                    resolve(null);
                } else {
                    return resolve('wa du hek');
                }

            } else {
                console.log(`getAvoidMultipleFRequest => else exception`);
                reject(err);
            }
        })
    })


}


//Post request for new friend
exports.requestforNewFriend = (req, res) => {

    let activeUid = req.body.activeUid;
    let activeName = req.body.activeName;
    let pasiveUid = req.body.pasiveUid;
    let pasiveName = req.body.pasiveName;

    getAvoidMultipleFRequest(activeUid, pasiveUid)
        .then((data) => {
            if (data) {
                postSended(activeUid, activeName, pasiveUid, pasiveName)
                    .then((data) => {
                        if (data) {
                            postRecived(activeUid, activeName, pasiveUid, pasiveName)
                                .then((data) => {
                                    console.log("postRecived => recived update");
                                    res.send(data)
                                })
                            console.log("mensaje a " + data);
                        } else {
                            res.status(400).send('PostSended function failed')
                        }
                    })
                    .catch((error) => {
                        console.log(error);
                        res.send(error)
                    })
            } else {
                res.status(400).send(`[${activeUid}:${activeUid}] already sent a friend request to: [${pasiveUid}:${pasiveName}]`)
                return;
            }
        })
        .catch((error) => {
            console.log(error)
            res.send(error)
        })
}
//Cancel request
exports.cancelFriendshipReq = (req, res) => {

    let activeUid = req.body.activeUid;
    let activeName = req.body.activeName;
    let pasiveUid = req.body.pasiveUid;
    let pasiveName = req.body.pasiveName;
    deleteSended(activeUid, activeName, pasiveUid, pasiveName)
        .then((data) => {
            if (data) {
                deleteRecived(activeUid, activeName, pasiveUid, pasiveName)
                    .then((data) => {
                        res.send(data)
                    })
                // socket.broadcast.to(data).emit('conectionLost', data);
            } else {
                res.status(400).send('deleteSended  => something went wrong deleteSended on cancelFriendshipReq ')
            }
        })
        .catch((error) => {
            console.log(error);
        })
}

//Get recibed friends request
exports.getRecivedFriendshipReq = (req, res) => {
    let playeruid = req.params.uid;
    createplayer.findOne({ uid: playeruid }, (err, response) => {
        if (err) return res.status(500).send({ message: ` getRecivedFriendshipReq Error al realizar la petición: ${err}` });
        if (!response) return res.status(404).send({ message: `getRecivedFriendshipReq No existe el jugador` });
        res.send(response.recivedFriendshipReq);
    });
};
//Get sended friends request
exports.getSendedFriendshipReq = (req, res) => {
    let playeruid = req.params.uid;
    createplayer.findOne({ uid: playeruid }, (err, response) => {
        if (err) return res.status(500).send({ message: `getSendedFriendshipReq Error al realizar la petición: ${err}` });
        if (!response) return res.status(404).send({ message: `getSendedFriendshipReq No existe el jugador` });
        res.send(response.sendedFriendshipReq);
    });
};

function addFriendToMyFList(user, frienduid, friendname) {
    return new Promise((resolve, reject) => {
        createplayer.findOne({ uid: frienduid }, function (err, response) {
            if (!response) {
                return resolve(null)
            }
            if (response) {
                createplayer.findOneAndUpdate({ uid: user },
                    {
                        $push: {
                            friends: {
                                uid: frienduid,
                                displayName: friendname
                            }
                        },
                        $pull: {
                            recivedFriendshipReq: {
                                $elemMatch: { uid: frienduid }
                            }
                        },
                        $inc: { version: -1 }
                    },
                    { new: true }, function (err, response) {
                        if (response) {
                            return resolve(response)
                        }
                        if (!response) {
                            return resolve(null)
                        } else {
                            reject(err)
                        }
                    })
            }
            else {
                reject(err)
            }
        })

    })
}

function addMyownIntoHisFList(frienduid, user, name) {
    return new Promise((resolve, reject) => {
        createplayer.findOneAndUpdate({ uid: frienduid },
            {
                $push: {
                    friends: {
                        uid: user,
                        displayName: name
                    }
                },
                $pull: {
                    sendedFriendshipReq: {
                        $elemMatch: { uid: user }
                    }
                }
            }, { new: true }, function (err, response) {
                if (response) {
                    return resolve(response)
                }
                if (!response) {
                    return resolve(null);
                }
                else {
                    reject(err)
                }
            })
    })
}
//Añade amigo 
exports.postaddNewFriend = (req, res) => {
    let user = req.body.user;
    let name = req.body.name;
    let frienduid = req.body.friendUid;
    let friendname = req.body.friendDisplayName;

    addFriendToMyFList(user, frienduid, friendname)
        .then((data) => {
            if (data) {
                datos = data
                console.log(`postaddNewFriend => [${friendname}:${frienduid}] Added into [${name}:${user} ]FriendList`)
                addMyownIntoHisFList(frienduid, user, name)
                    .then((data) => {
                        if (data) {
                            console.log(`postaddNewFriend => [${name}:${user}] Added into [${friendname}:${frienduid}] FriendList`)
                            res.send(datos);
                            return;
                        } else {
                            console.log('postAddNewfriend  => not worked P2 ')
                            res.status(400).send('postAddNewfriend => not worked P2')
                            return;
                        }
                    })
                    .catch((err) => { console.log(err), res.send(err) })
            } else {
                console.log('postAddNewfriend => not worked P1 ')
                res.status(400).send('postAddNewfriend => not worked P1 ')
                return;
            }
        })
        .catch((err) => { console.log(err), res.send(error) })

}
//Muestra la lista de tus amigos

exports.postMyFriends = (req, res) => {
    let user = req.body.user;

    createplayer.findOne({ uid: user }, function (err, response) {
        if (response) {
            res.send(response.friends);
            return;
        } else if (!response) {
            console.log("postMyFriends=> no se ha encontrado el jugador");
            return;
        } else {
            console.log(`postMyFriends=> Ha ocurrido un error ${err}`);
            res.send(err)
        }
    })
}
//Muestra perfil de tu amgigo
exports.postFriendProfile = (req, res) => {
    let friendUid = req.body.frienduid;

    createplayer.findOne({ uid: friendUid }, function (err, response) {

        if (response) {
            var responseObject = response.toJSON();
            var removeObjectProperties = function (obj, props) {

                for (var i = 0; i < props.length; i++) {
                    if (obj.hasOwnProperty(props[i])) {
                        delete obj[props[i]];
                    }
                }

            };
            removeObjectProperties(responseObject, ["friends", "email", "recivedFriendshipReq", "sendedFriendshipReq", "SocketId"]);


            res.send(responseObject);
            return;
        } else if (!response) {
            console.log(`postFriendProfile  => user: ${friendUid} profile not found`);
            res.status(404).send('profile not found');
            return;
        } else if (err) {
            console.log(`postFriendProfile => something went wrong ${err}`);
            res.send(err)

        }
    })
}
exports.getAllPlayers = (req, res) => {
    let userUid = req.params.uid
    createplayer.find({}, (err, result) => {
        if (result) {
            resultado = result

            createplayer.findOne({ uid: userUid }, (err, result) => {
                if (result) {
                    const UidAndDisplayName = [];
                    for (i = 0; i < resultado.length; i++) {
                        UidAndDisplayName.push({ "displayName": resultado[i].displayName, "uid": resultado[i].uid, "photoURL": resultado[i].photoURL })
                    }
                    const pushPeople = [];
                    pushPeople.push(userUid);
                    const myPlayer = resultado.find(o => o.uid === userUid);
                    getAllMyPeople(myPlayer.friends, pushPeople);
                    getAllMyPeople(myPlayer.recivedFriendshipReq, pushPeople);
                    getAllMyPeople(myPlayer.sendedFriendshipReq, pushPeople);


                    let Slice = UidAndDisplayName.slice(0);

                    Slice.forEach((element) => {
                        if (pushPeople.includes(element['uid'])) {
                            let removeUid = UidAndDisplayName.map((item) => item['uid']).indexOf(element['uid']);
                            UidAndDisplayName.splice(removeUid, 1);
                        }

                    })

                    console.log('getAllPLayers => Lista de usuarios ajenos enviada: ° ͜ʖ ͡° ')

                    res.send(UidAndDisplayName)
                }
                if (!result) {
                    console.log(`getAllPLayers => No existe ningun usuario con la uid: ${userUid}`);
                    res.status(404).send(`getAllPLayers => No existe ningun usuario con la uid: ${userUid}`)


                } else if (err) { console.log(err), res.send(err) }

            })


        }
        if (!result) {
            console.log("getAllPLayers => No ha habido resultado ¯_(ツ)_/¯ ");
            res.status(400).send('getAllPLayers => No ha habido resultado ¯_(ツ)_/¯')
        }
        if (err) {
            res.send(err)
        }
    })
}
exports.editPlayerPhoto = function (req, res) {
    let user = req.params.uid
    let url = req.body.photoURL
    let imgId = req.body.idPhoto
    createplayer.findOneAndUpdate({ uid: user }, { photoURL: url, idPhoto: imgId }, { new: true }, function (err, result) {
        if (!err) {
            res.send(result)
        }
        else if (err) {
            console.log("editName error")
            return res.send(`Server error ${err}`);
        }
    });
}

exports.postRemoveFriend = function (req, res) {
    let friendUid = req.body.frienduid;
    let MyUid = req.body.uid;

    createplayer.findOneAndUpdate({ uid: MyUid, friends: { $elemMatch: { uid: friendUid } } },
        { $pull: { friends: { uid: friendUid } } },
        { new: true }, function (err, response) {
            if (response) {
                console.log(`friend ${friendUid} deleted in ${MyUid}`)
                createplayer.findOneAndUpdate({ uid: friendUid, friends: { $elemMatch: { uid: MyUid } } },
                    { $pull: { friends: { uid: MyUid } } },
                    { new: true }, function (err, response) {
                        if (response) {
                            console.log(`friend ${MyUid} deleted in ${friendUid}`);
                            res.status(200).send('200');
                            return;
                        }
                        if (!response) {
                            console.log('Removefriend => No response in 2nd part ');
                            res.status(404).send('404');
                            return;
                        }
                        if (err) {
                            console.log(`Removefriend => ERR: ${err}`);
                            res.send(err);
                            return;
                        }
                    }
                )
            }
            if (!response) {
                console.log(`Removefriend => 404, no hay resultados`);
                res.status(404).send('404');
                return;
            }
            if (err) {
                console.log(err);
                res.send(err);
                return;
            }
        }
    );
}

// helper
function getAllMyPeople(fromWhere, whereDoIPush) {
    for (i = 0; i < fromWhere.length; i++) {
        whereDoIPush.push(fromWhere[i].uid)
    }
}
//Function set socket to model
exports.postSocketId = function (req, res) {
    let uid = req.body.uid;
    let SocketId = req.body.SocketId;
    createplayer.findOneAndUpdate({ uid: uid }, { SocketId: SocketId }, { new: true }, function (err, result) {
        if (result) {
            // console.log(result)
            return res.status(200).send({ message: 'state update OK' });
        }
        if (err) return res.status(500).send({ message: 'state update 500' })
    });

}
//Function delete socket from model

exports.findBysocketIdandDelete = (mySocket) => {
    console.log('entrar a borrar el socket');
    createplayer.findOneAndUpdate({ SocketId: mySocket }, { SocketId: null },
        { new: true }, function (err, result) {
            if (err) console.log('findBysocketIdandDelete  => error deleting dat socketid');
            if (result) {
                console.log('findBysocketIdandDelete  => socket eliminado conrrectamente ');
            }
        });
}
//Function compare version
exports.compareVersion = function (req, res) {

    let uid = req.body.uid;
    let version = req.body.version
    let flagDiferentVersion = false;
    let flagDessincronyzeVersion = false;
    let resultDesinc;
    createplayer.findOne({ uid: uid }, function (err, result) {
        if (err) {
            console.log('compareVersion  => error')
        }
        return new Promise((resolve, reject) => {
            if ((result.recivedFriendshipReq.length != result.version) || (version != 0)) {
                createplayer.findOneAndUpdate({ uid: uid }, { version: result.recivedFriendshipReq.length }, { new: true }, function (err, result) {
                    if (err) {
                        console.log('cant set version  => error deleting dat version')
                        reject(err)
                    };
                    if (result) {
                        resultDesinc = result
                        resolve(flagDessincronyzeVersion = true);
                    }
                });
            }
            else {
                resultDesinc = result;
                resolve(flagDessincronyzeVersion);
            }
        })
            .then((flagDessincronyzeVersion) => {
                if (resultDesinc && resultDesinc.version > 0) {
                    flagDiferentVersion = true;
                }
                console.log('flagDiferentVersion ' + flagDiferentVersion, 'flagDessincronyzeVersion ' + flagDessincronyzeVersion)
                if (!flagDiferentVersion && flagDessincronyzeVersion) {
                    return res.send({ result: resultDesinc, notification: null });
                }
                else if (flagDiferentVersion && !flagDessincronyzeVersion) {
                    return res.send({ result: null, notification: 'notification button change' })
                }
                else if (flagDiferentVersion && flagDessincronyzeVersion) {
                    return res.send({ result: resultDesinc, notification: 'notification button change' })
                }
                else return res.send({ result: null, notification: 'null' })
            })
    })
}