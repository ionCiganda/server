const express = require ('express')
const router = express.Router();
const matchCtrl = require ('../controllers/drawControllers')
const wordCntrl = require ('../controllers/wordController')
const playerCntrl = require ('../controllers/playerController')

//metodos matchgame
router.get('/match/:id', matchCtrl.getMatchByid);
router.delete('/match/:id', matchCtrl.DeleteMatchByid);
router.put('/match/:id', matchCtrl.putMatch);
//metodo busqueda partidas abiertas(con jugador1 y sin jugador2)
router.post('/newmatch/', matchCtrl.searchOrCreateMatch);
//metodos word
router.get('/word/:tema', wordCntrl.getWords);
router.get('/word', wordCntrl.getAllwords);

// private match methods
router.post('/privateMatch/', matchCtrl.createPrivateMatch);
router.post('/joinprivate/', matchCtrl.joinPrivateMatch);

//metodos player
router.post('/player',playerCntrl.postPlayer);
router.get('/player/:id', playerCntrl.getPlayerByid);
router.delete('/player/:id', playerCntrl.DeletePlayerByid);
router.put('/player/:id', playerCntrl.putPlayer);
router.post('/newUser',playerCntrl.postNewUser);
router.post('/editName/:uid',playerCntrl.editPlayerName);
router.get('/getAllUsers/:uid', playerCntrl.getAllPlayers);
router.post('/requestforNewFriend/', playerCntrl.requestforNewFriend);
router.get('/recivedFriendshipReq/:uid', playerCntrl.getRecivedFriendshipReq);
router.get('/sendedFriendshipReq/:uid', playerCntrl.getSendedFriendshipReq);
router.post('/cancelFriendshipReq/', playerCntrl.cancelFriendshipReq);
router.post('/addNewFriend', playerCntrl.postaddNewFriend);
router.post('/postMyFriends', playerCntrl.postMyFriends);
router.post('/friendProfile', playerCntrl.postFriendProfile);
router.get('/getAllUsers/', playerCntrl.getAllPlayers);
router.post('/updatePhoto/:uid', playerCntrl.editPlayerPhoto);
router.post('/postRemoveFriend/', playerCntrl.postRemoveFriend);
//Prueba change status
router.post('/changeStatus/', playerCntrl.postSocketId)
router.post('/getVersion/', playerCntrl.compareVersion)
module.exports = router;

