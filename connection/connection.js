const http = require('http');
const express = require('express');
const socketIO = require('socket.io');
const port = process.env.PORT || 3002;
const app = express();
const server = http.createServer(app);
const io = socketIO(server, { pingTimeout: 3000, pingInterval: 2000 });
// Route mongo
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const mongodbRoute = 'mongodb://Admin:AplAeg_2018@ds157864.mlab.com:57864/drawmach'
const router = require("../routes/router");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
})

app.use(router);

/*MONGODB*/
const options = {
    socketTimeoutMS: 0,
    keepAlive: true,
    reconnectTries: 30,
    useNewUrlParser: true
};
mongoose.Promise = global.Promise
mongoose.connect(mongodbRoute, options, (err) => {
    if (err) {
        return console.log(`Error al conectar a la base de datos: ${err}`)
    }
    server.listen(port, () => {
        console.log(`Server activo en ${port}`);
    });

    console.log(`Conexión con Mongo correcta.`)
})


exports.ioConnection = () => {
    return io;
};