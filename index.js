const connection = require('./connection/connection');
const funciones = require("./helpers/funciones");
const io = require('./connection/connection');
const playerCntrl = require ('./controllers/playerController');

ioConnection = io.ioConnection();

ioConnection.on('connection', (socket) => {
  
    console.log(`Usuario conectado en socket: ${socket.id}`);
    socket.emit('conectado', { user: socket.id });
    
    socket.on('lobbyBack', function (data) {
        console.log("enviar a " + data.uuid);
        funciones.RemovePlayerfromMatch(data.uuid);
    });

    socket.on('lobbyReady', function (data) {
        console.log("enviar a " + data.uuid + data.id);
        funciones.putListo(data);
    });

    socket.on('imagecompleted', function (data) {
        console.log("soy" + data.socketid);
        if (data.socketid == data.partida.socketID1) {
            var socketVisor = data.partida.socketID2
        }
        else {
            var socketVisor = data.partida.socketID1
        }
        socket.broadcast.to(socketVisor).emit('sendDibujo', { planos: data.planos });
        //socket.broadcast.to(data.socketid).emit('senddibujo', {planos:data.planos,sessionStorageData:data.sessionStorageData});
    });

    socket.on('wordSelected', function (data) {
        console.log("word selected" + data.word + "socket pintor" + data.pintor);
        console.log(data.data);
        funciones.UpdateWordMacth(data);
    });

    socket.on('validateWord', function (data) {
        funciones.ValidateWord(data);
    });

    socket.on('disconnect', () => {
        console.log(`Usuario desconectado: ${socket.id}`);
        let mySocket = socket.id;
        console.log(mySocket);
        playerCntrl.findBysocketIdandDelete(mySocket);
        funciones.RemovePlayerfromMatchBySocketId(socket.id)
            .then((data) => {               
                console.log("control2" + data);
                if (data) {
                    console.log("mensaje a " + data);
                    socket.broadcast.to(data).emit('conectionLost', data);
                }
            })
            .catch((error) => {
                console.log(error);
            })        
    });
});