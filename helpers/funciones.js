const creatematch = require('../models/match');
const createplayer = require('../models/player');

//Para usar en la perdida de una conexion de socket
exports.RemovePlayerfromMatchBySocketId = ((socketid) => {
    return new Promise((resolve, reject) => {
        //Intentamos borrar la partida en la que esté el jugador desconectado como jugador1
        creatematch.findOneAndDelete({ $or: [{ socketID1: socketid }, { socketID2: socketid }] }, (err, result) => {
            console.log("resultado " + result);
            var data;
            if (result) {
                console.log("está como player " + result);
                if (socketid == result.socketID1) {
                    console.log("entra1 " + result.socketID2);
                    data = result.socketID2;    
                 
                }
                else {
                    console.log("entra2 " + result.socketID1);
                    data = result.socketID1;               
                   
                }
            }
            if (err) {
                console.log("Error1" + err);
                reject(err)
            }
            console.log(data);
            resolve(data);
        })
    })
})

//Para cuando un jugador se sale del lobby
exports.RemovePlayerfromMatch = ((uuid) => {
    creatematch.findOneAndDelete({ $or: [{ player1: uuid }, { player2: uuid }] }, function (err, response) {
        if (response) {
            ioConnection.to(`${response.socketID1}`).emit('playerLeft', { navigate: '/home/' })
            ioConnection.to(`${response.socketID2}`).emit('playerLeft', { navigate: '/home/' })
        }

        if (err) {
            console.log(`Server error ${err}`);
        }
    })
})

//Funciona a la que llamamos cuando un jugador le da al boton listo en el lobby
exports.putListo = ((data) => {
    if (data.uuid == data.match.player1) {
        var newListoP1 = !data.match.listoP1;
        var newListoP2 = data.match.listoP2;
    }
    else if (data.uuid == data.match.player2) {
        var newListoP2 = !data.match.listoP2;
        var newListoP1 = data.match.listoP1;
    }
    creatematch.findOneAndUpdate({ _id: data.match._id }, { listoP1: newListoP1, listoP2: newListoP2 }, { new: true }, function (err, result) {
        if (result) {
            console.log("resultado FINDONEANDUPDATE");
            console.log(result);
            if (result.listoP1 == true && result.listoP2 == true) {
                ioConnection.to(`${result.socketID1}`).emit('navigate', { navigate: '/visor/' });
                ioConnection.to(`${result.socketID2}`).emit('navigate', { navigate: '/palabra/' });
            }
            else {
                ioConnection.to(`${result.socketID1}`).emit('ticks', result);
                ioConnection.to(`${result.socketID2}`).emit('ticks', result);
            }
            return console.log({ match: result });
        }
        //si la uuid es del jugador 2 y ha clickado el boton listo        
        if (err) return console.log(`Server error ${err}`);
    })
})

//Funcion para actualizar la palabra seleccionada por el pintor
exports.UpdateWordMacth = ((data) => {
    creatematch.findByIdAndUpdate(data.data._id, { word: data.word, pista: data.tema }, { new: true }, function (err, result) {
        if (result) {
            if (data.pintor == result.socketID1) {
                ioConnection.to(`${result.socketID1}`).emit('wordUpdated', { partida: result, navigate: '/pintor/' });
                ioConnection.to(`${result.socketID2}`).emit('wordUpdated', { partida: result, navigate: '/visor/' });
            }
            else {
                ioConnection.to(`${result.socketID2}`).emit('wordUpdated', { partida: result, navigate: '/pintor/' });
                ioConnection.to(`${result.socketID1}`).emit('wordUpdated', { partida: result, navigate: '/visor/' });
            }
        }
        if (err) {
            return console.log(`Server error ${err}`);
        }
    });
})
//Actualizamos las estadisticas de cada jugador y se las mandamos para almacenarlas en el localstorage junto con el mensaje de fin de partidas 
//Borramos la partida
function actualizarEstadisticas(playerPintor, playerVisor, acierto, socketVisor, socketPintor, matchId, word) {
    console.log('Actualizar estadisticas');
    var numAciertosPintor = 0;
    var numAciertosVisor = 0;
    var mensajePintor = "Time is up!!!";
    var mensajeVisor = "Time is up!!! Word was: ";

    if (acierto) {
        mensajePintor = "Correct word";
        mensajeVisor = "Correct word: ";
        numAciertosPintor = 1;
        numAciertosVisor = 1;
    }
    console.log(numAciertosVisor + '/' + numAciertosPintor)

    createplayer.findOneAndUpdate({ uid: playerVisor }, {
        $inc: { partidasVisor: 1, aciertosVisor: numAciertosVisor },
    },
        { new: true }, (err, result) => {
            if (result) {
                console.log('pre result V');
                console.log(result);
                ioConnection.to(`${socketVisor}`).emit('correctWord', { message: mensajeVisor + word, player: result });
                return result;
            }
            if (err) reject(err);
        })

    createplayer.findOneAndUpdate({ uid: playerPintor }, {
        $inc: { partidasPintor: 1, aciertosPintor: numAciertosPintor },
    },
        { new: true }, (err, result) => {
            if (result) {
                console.log('pre result P');
                console.log(result);
                ioConnection.to(`${socketPintor}`).emit('correctWord', { message: mensajePintor, player: result });
                return result;
            }
            if (err) reject(err);
        })

    creatematch.findByIdAndDelete({ _id: matchId }, (err, result) => {
        if (err) return console.log(`Error al realizar la peticion: ${err}`);
        if (!result) return console.log(`No existe la partda`);
        console.log(`Deleted correctly`);
    });
}

//Validamos la palabra del cliente visor
exports.ValidateWord = ((data) => {

    if (data.socketid == data.match.socketID1) {
        var otherPlayerSocketId = data.match.socketID2
        var uidVisor = data.match.player1
        var uidPintor = data.match.player2
    }
    else {
        var otherPlayerSocketId = data.match.socketID1
        var uidVisor = data.match.player2
        var uidPintor = data.match.player1
    }
    if (data.timeOut == 1) {
        creatematch.findOne({ _id: data.match._id }, (err, result) => {
            if (result) {
                actualizarEstadisticas(uidPintor, uidVisor, false, data.socketid, otherPlayerSocketId, data.match._id, data.match.word);
            }
            if (err) return res.status(500).send({ message: `Error al realizar la petición: ${err}` });
        })
    }

    else if (data.timeOut == 0) {
        creatematch.findOne({ _id: data.match._id }, (err, result) => {
            console.log('entra en response')
            if (err) return res.status(500).send({ message: `Error al realizar la petición: ${err}` });
            if (result) {
                console.log("data " + result)
                console.log("data " + result.word)
                if (result.word.toUpperCase() == data.value.toUpperCase()) {
                    actualizarEstadisticas(uidPintor, uidVisor, true, data.socketid, otherPlayerSocketId, data.match._id, data.match.word);
                }
            }
        });
    }
});



